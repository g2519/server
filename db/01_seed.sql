/******************************
Author:     Tharkaesh Pulikonda
CreatedOn:  04-Nov-2021
Version:    0.1
******************************/

begin;
    delete from projects;
    insert into projects(unique_code, description) values ('gsk', 'gsk');
end;