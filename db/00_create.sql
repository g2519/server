/******************************
Author:     Tharkaesh Pulikonda
CreatedOn:  04-Nov-2021
Version:    0.1
******************************/

-- create database gsk_pmt;
begin;

drop table if exists project_employees;
drop table if exists employees;
drop table if exists projects;

create table projects (
    id              SERIAL PRIMARY KEY,
    "name"          character varying(80) not null unique,
    unique_code     character varying(3) not null unique default upper(substring(md5(random()::text), 0, 4)),
    description		character varying(300) not null,
    is_active       boolean not null default true,
    start_date      timestamp with time zone NOT null default now(),
    created_on      timestamp with time zone NOT null default now(),
    updated_on      timestamp with time zone,
    created_by      bigint,
    updated_by      bigint
);

create table employees (
    id              SERIAL PRIMARY KEY,
    email           character varying(512) unique not null,
    full_name       character varying(50) not null,
    unique_code     character varying(6) not null unique default upper(substring(md5(random()::text), 0, 7)),
    is_active       boolean not null default true,
    created_on      timestamp with time zone NOT null default now(),
    updated_on      timestamp with time zone,
    created_by      bigint,
    updated_by      bigint
);

create table project_employees (
    id              SERIAL PRIMARY KEY,
    project_id      bigint not null,
    employee_id     bigint not null,
    created_on      timestamp with time zone NOT null default now(),
    created_by      bigint
);

-- foreign key relationships
alter table "public".project_employees add constraint fk_project_employees_project_id foreign key ("project_id") references "public".projects("id") DEFERRABLE INITIALLY DEFERRED;
alter table "public".project_employees add constraint fk_project_employees_employee_id foreign key ("employee_id") references "public".employees("id") DEFERRABLE INITIALLY DEFERRED;

-- unique constraints
ALTER TABLE "public".project_employees ADD CONSTRAINT uq_project_employees unique(project_id, employee_id);

end;