import { Entity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, Column, ManyToMany, JoinTable, OneToMany } from 'typeorm';
import { EmployeeEntity } from './employee.entity';

@Entity({ name: 'projects', schema: 'public' })
export class ProjectEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 80, unique: true, nullable: false })
  name: string;

  @Column({ type: 'varchar', length: 300, nullable: false })
  description: string;

  @Column({ type: 'varchar', length: 3, nullable: false })
  unique_code: string;

  @Column({ type: 'boolean', default: true, nullable: false })
  is_active: boolean;

  @Column({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP', nullable: false })
  start_date: Date;

  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  created_on: Date;

  @UpdateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  updated_on: Date;

  @Column({ type: 'bigint', default: () => null })
  created_by: number;

  @Column({ type: 'bigint', default: () => null })
  updated_by: number;

  @ManyToMany(type => EmployeeEntity)
  @JoinTable({
    name: "project_employees",
    joinColumn: {
      name: 'project_id'
    },
    inverseJoinColumn: {
      name: "employee_id"
    }
  })
  employees: EmployeeEntity[];
}