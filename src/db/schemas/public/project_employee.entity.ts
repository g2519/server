import { Entity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, Column } from 'typeorm';

@Entity({ name: 'project_employees', schema: 'public' })
export class ProjectEmployeeEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'bigint',  nullable: false  })
  project_id: number;

  @Column({ type: 'bigint',  nullable: false  })
  employee_id: number;

  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  created_on: Date;

  @Column({ type: 'bigint', default: () => null })
  created_by: number;
}