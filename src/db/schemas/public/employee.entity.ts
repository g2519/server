import { Entity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, Column, ManyToMany, JoinTable } from 'typeorm';
import { ProjectEntity } from './project.entity';

@Entity({ name: 'employees', schema: 'public' })
export class EmployeeEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 512, unique: true, nullable: false })
  email: string;

  @Column({ type: 'varchar', length: 50, nullable: false })
  full_name: string;

  @Column({ type: 'varchar', length: 6, nullable: false })
  unique_code: string;

  @Column({ type: 'boolean', default: true, nullable: false })
  is_active: boolean;

  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  created_on: Date;

  @UpdateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  updated_on: Date;

  @Column({ type: 'bigint', default: () => null })
  created_by: number;

  @Column({ type: 'bigint', default: () => null })
  updated_by: number;

  @ManyToMany(type => ProjectEntity)
  @JoinTable({
    name: "project_employees",
    joinColumn: {
      name: 'project_id'
    },
    inverseJoinColumn: {
      name: "employee_id"
    }
  })
  projects: ProjectEntity[];
}