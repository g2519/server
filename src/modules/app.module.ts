import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { InitDB } from './app.dbconfig';
import { AppController } from './app.controller';
import { AppService } from './app.service';

import { ProjectModule } from "./project/project.module";
import { EmployeeModule } from "./employee/employee.module";

@Module({
  imports: [ConfigModule.forRoot(), TypeOrmModule.forRoot(InitDB()), ProjectModule, EmployeeModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
