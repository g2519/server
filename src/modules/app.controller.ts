import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('/api')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  ping(): Object {
    return {
      server_time: Date.now(),
      app_name: process.env.APP_NAME
    }
  }
}
