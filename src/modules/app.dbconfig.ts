import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import * as path from 'path';

export function InitDB(): TypeOrmModuleOptions {
    return ({
        type: "postgres",
        host: process.env.DB_HOST,
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        entities: [path.join(__dirname, '../db/schemas/**/*.entity{.ts,.js}')],
        //synchronize: true,
        logging: (process.env.DB_LOGGING == '1'),
        logger: "advanced-console",
    });
}
