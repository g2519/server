import { Param, Body, Controller, Get, Post, Delete, Put, HttpException, HttpStatus } from '@nestjs/common';
import { CreateProjectDto, UpdateProjectDto, AssignEmployeesDto } from "./dto";
import { ProjectService } from './project.service';

@Controller('/api/projects')
export class ProjectController {
  constructor(private readonly projectService: ProjectService) { }

  @Get()
  async list(): Promise<any> {
    return await this.projectService.getAllAsync();
  }

  @Get(':id')
  async getById(@Param('id') id?: number): Promise<any> {
    return await this.projectService.getById(id);
  }
  
  @Post()
  async create(@Body() createProjectDto: CreateProjectDto): Promise<any> {
    const isExists = await this.projectService.isNameExists(createProjectDto.name);
    if (isExists) {
      throw new HttpException({ status: HttpStatus.FORBIDDEN, error: "Another project with the same exists" }, HttpStatus.FORBIDDEN);
    }
    return await this.projectService.createAsync(CreateProjectDto.toEntity(createProjectDto));
  }

  @Put(':id')
  async update(@Param('id') id: number, @Body() updateProjectDto: UpdateProjectDto): Promise<any> {
    const isExists = await this.projectService.isNameExists(updateProjectDto.name, id);
    if (isExists) {
      throw new HttpException({ status: HttpStatus.FORBIDDEN, error: "Another project with the same exists" }, HttpStatus.FORBIDDEN);
    }
    return await this.projectService.updateAsync(id, UpdateProjectDto.toEntity(id, updateProjectDto));
  }

  @Post(':id/assign')
  async assign(@Param('id') id: number,@Body() assignEmployeesDto: AssignEmployeesDto): Promise<any> {
    return await this.projectService.assignAsync(id, assignEmployeesDto.employees);
  }

  @Post(':id/deassign')
  async deassign(@Param('id') id: number,@Body() assignEmployeesDto: AssignEmployeesDto): Promise<any> {
    return await this.projectService.deassignAsync(id, assignEmployeesDto.employees);
  }
}
