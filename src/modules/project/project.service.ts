import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { ProjectEntity } from '../../db/schemas/public/project.entity';
import { ProjectEmployeeEntity } from '../../db/schemas/public/project_employee.entity';
import { EmployeeEntity } from '../../db/schemas/public/employee.entity';

@Injectable()
export class ProjectService {
  constructor(@InjectRepository(ProjectEntity) private readonly projectDao: Repository<ProjectEntity>,
    @InjectRepository(ProjectEmployeeEntity) private readonly projectEmployeeDao: Repository<ProjectEmployeeEntity>,
    @InjectRepository(EmployeeEntity) private readonly employeeDao: Repository<EmployeeEntity>) { }

  async getAllAsync(): Promise<ProjectEntity[]> {
    return await this.projectDao.find({ order: { id: "DESC" } });
  }

  async getById(project_id: number): Promise<ProjectEntity> {
    let project = await this.projectDao.findOne(project_id, { relations: ["employees"] });
    if (!project) {
      throw "Project with this id not found";
    }
    return project;
  }

  async createAsync(item: ProjectEntity): Promise<ProjectEntity> {
    return await this.projectDao.save(item);
  }

  async updateAsync(id: number, item: ProjectEntity): Promise<ProjectEntity> {
    const result: UpdateResult = await this.projectDao.update({ id }, item);
    return await this.getById(id);
  }

  async isNameExists(name: string, id?: number): Promise<boolean> {
    let record = null;
    if (!id) {
      record = await this.projectDao.createQueryBuilder().where("LOWER(name) = LOWER(:name)", { name }).getOne();
      return record != null;
    }
    record = await this.projectDao.createQueryBuilder().where("LOWER(name) = LOWER(:name) and id != :id", { name, id }).getOne();
    return record != null;
  }

  async assignAsync(project_id: number, employees: number[]) {
    for (const employee_id of employees) {
      let record = await this.projectEmployeeDao.findOne({ project_id, employee_id });
      if (!record) {
        const new_record = new ProjectEmployeeEntity();
        new_record.employee_id = employee_id;
        new_record.project_id = project_id;
        await this.projectEmployeeDao.save(new_record);
      }
    }
    return this.getById(project_id);
  }

  async deassignAsync(project_id: number, employees: number[]) {
    for (const employee_id of employees) {
      let record = await this.projectEmployeeDao.findOne({ project_id, employee_id });
      if (record) {
        await this.projectEmployeeDao.delete({ id: record.id });
      }
    }
    return this.getById(project_id);
  }
}
