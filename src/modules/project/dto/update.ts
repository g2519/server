import { IsOptional, IsNotEmpty, MinLength, MaxLength, IsDateString, IsAlphanumeric } from 'class-validator';
import { ProjectEntity } from "../../../db/schemas/public/project.entity"

export default class UpdateProjectDto {
  @IsOptional()
  id: number;

  @IsNotEmpty()
  @IsAlphanumeric()
  name: string;

  @IsOptional()
  @MinLength(50)
  @MaxLength(300)
  description: string;

  public static toEntity(id: number, dto: UpdateProjectDto): ProjectEntity {
     const project  = new ProjectEntity();
     project.id = id;
     project.name = dto.name;
     project.description = dto.description;
     return project;
  }
}