// import { ApiModelProperty } from '@nestjs/swagger';
import { IsNumber, ArrayMinSize, ArrayMaxSize } from 'class-validator';

export default class AssignEmployeesDto {
  @IsNumber({},{each: true})
  @ArrayMinSize(1)
  @ArrayMaxSize(10)
  employees: number[];
}