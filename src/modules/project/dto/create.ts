// import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty, MinLength, MaxLength, IsDateString, IsAlphanumeric } from 'class-validator';
import { ProjectEntity } from "../../../db/schemas/public/project.entity"

export default class CreateProjectDto {
  @IsNotEmpty()
  @IsAlphanumeric()
  @MinLength(10)
  @MaxLength(80)
  name: string;

  @IsNotEmpty()
  @MinLength(50)
  @MaxLength(300)
  description: string;

  @IsNotEmpty()
  @IsDateString()
  start_date: string;

  public static toEntity(dto: CreateProjectDto): ProjectEntity {
     const project  = new ProjectEntity();
     project.name = dto.name;
     project.description = dto.description;
     project.start_date = new Date(dto.start_date);
     return project;
  }
}