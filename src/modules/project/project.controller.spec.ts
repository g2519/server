import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectController } from './project.controller';
import { ProjectService } from './project.service';
import { ProjectEmployeeEntity } from '../../db/schemas/public/project_employee.entity';
import { ProjectEntity } from '../../db/schemas/public/project.entity';
import { EmployeeEntity } from '../../db/schemas/public/employee.entity';
import { CreateProjectDto } from "./dto"
import { InitDB } from '../app.dbconfig';
jest.setTimeout(1000 * 60);

const projectEntityMockObject: ProjectEntity = {
    id: 1, 
    name: 'test',
    description: 'test', 
    start_date: new Date(), 
    is_active: true, 
    unique_code: '123',
    created_on: new Date(),
    updated_on: null,
    updated_by: null,
    created_by: null,
    employees: []
};

describe('ProjectController', () => {
    let projectController: ProjectController;
    let projectService: ProjectService;
    let app: TestingModule

    beforeEach(async () => {
        app = await Test.createTestingModule({
            imports: [
                ConfigModule.forRoot(), 
                TypeOrmModule.forRoot(InitDB()), 
                TypeOrmModule.forFeature([ProjectEntity, ProjectEmployeeEntity, EmployeeEntity]), ],
            controllers: [ProjectController],
            providers: [ProjectService],
        }).compile();

        projectService = app.get<ProjectService>(ProjectService);
        projectController = app.get<ProjectController>(ProjectController);
    });

    afterEach(async () => {
        await app.close();
    });

    describe('/api/projects', () => {
        it('GET return an array of projects', async () => {
            const projects: ProjectEntity[] = [projectEntityMockObject];
            jest.spyOn(projectService, 'getAllAsync').mockImplementation(async () => projects);

            expect(await projectController.list()).toBe(projects);
        });

        it('GET BY ID: should return one project if exists', async () => {
            
            jest.spyOn(projectService, 'getById').mockImplementation(async () => projectEntityMockObject);
            expect(await projectController.getById()).toBe(projectEntityMockObject);
        });
    });
});
