import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectService } from './project.service';
import { ProjectController } from './project.controller';
import { ProjectEntity } from '../../db/schemas/public/project.entity';
import { ProjectEmployeeEntity } from '../../db/schemas/public/project_employee.entity';
import { EmployeeEntity } from '../../db/schemas/public/employee.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ProjectEntity, ProjectEmployeeEntity, EmployeeEntity]), ],
  controllers: [ProjectController],
  providers: [ProjectService],
  exports: []
})
export class ProjectModule {}
