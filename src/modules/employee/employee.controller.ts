import { Param, Body, Controller, Get, Post, Delete, Put, HttpException, HttpStatus } from '@nestjs/common';
import { UpdateEmployeeDto, CreateEmployeeDto } from "./dto";
import { EmployeeService } from './employee.service';

@Controller('/api/employees')
export class EmployeeController {
  constructor(private readonly _service: EmployeeService) { }

  @Get()
  async list(): Promise<any> {
    return await this._service.getAllAsync();
  }

  @Get(':id')
  async getById(@Param('id') id?: number): Promise<any> {
    return await this._service.getById(id);
  }
  
  @Post()
  async create(@Body() createDto: CreateEmployeeDto): Promise<any> {
    const isExists = await this._service.isEmailExists(createDto.email);
    if (isExists) {
      throw new HttpException({ status: HttpStatus.FORBIDDEN, error: "Another employee with the same email exists" }, HttpStatus.FORBIDDEN);
    }
    return await this._service.createAsync(CreateEmployeeDto.toEntity(createDto));
  }

  @Put(':id')
  async update(@Param('id') id: number, @Body() updateDto: UpdateEmployeeDto): Promise<any> {
    const isExists = await this._service.isEmailExists(updateDto.email, id);
    if (isExists) {
      throw new HttpException({ status: HttpStatus.FORBIDDEN, error: "Another employee with the same email exists" }, HttpStatus.FORBIDDEN);
    }
    return await this._service.updateAsync(id, UpdateEmployeeDto.toEntity(id, updateDto));
  }
}
