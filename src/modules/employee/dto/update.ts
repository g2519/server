// import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty, IsOptional, IsEmail } from 'class-validator';
import { EmployeeEntity } from "../../../db/schemas/public/employee.entity"

export default class UpdateEmployeeDto {
  @IsOptional()
  id: number;

  @IsNotEmpty()
  full_name: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;  

  public static toEntity(id: number, dto: UpdateEmployeeDto): EmployeeEntity {
     const employee  = new EmployeeEntity();
     employee.id = id;
     employee.full_name = dto.full_name;
     employee.email = dto.email;
     return employee;
  }
}