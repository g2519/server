// import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty, IsEmail } from 'class-validator';
import { EmployeeEntity } from "../../../db/schemas/public/employee.entity"

export default class CreateEmployeeDto {
  @IsNotEmpty()
  full_name: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  public static toEntity(dto: CreateEmployeeDto): EmployeeEntity {
     const employee  = new EmployeeEntity();
     employee.email = dto.email;
     employee.full_name = dto.full_name;
     return employee;
  }
}