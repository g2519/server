import CreateEmployeeDto from "./create";
import UpdateEmployeeDto from "./update";
export {
    CreateEmployeeDto,
    UpdateEmployeeDto
}