import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { EmployeeEntity } from '../../db/schemas/public/employee.entity';

@Injectable()
export class EmployeeService {
  constructor(@InjectRepository(EmployeeEntity) private readonly _dao: Repository<EmployeeEntity>) { }

  async getAllAsync(): Promise<EmployeeEntity[]> {
    return await this._dao.find({ order: { id: "DESC" } });
  }

  async getById(id: number): Promise<EmployeeEntity> {
    return await this._dao.findOne(id, { relations: ["projects"]});
  }

  async createAsync(item: EmployeeEntity): Promise<EmployeeEntity> {
    return await this._dao.save(item);
  }

  async updateAsync(id: number, item: EmployeeEntity): Promise<EmployeeEntity> {
    const result: UpdateResult = await this._dao.update({ id }, item);
    return await this.getById(id);
  }

  async isEmailExists(email: string, id?: number): Promise<boolean> {
    let record = null;
    if (!id) {
      record = await this._dao.createQueryBuilder().where("LOWER(email) = LOWER(:email)", { email }).getOne();
      return record != null;
    }
    record = await this._dao.createQueryBuilder().where("LOWER(email) = LOWER(:email) and id != :id", { email, id }).getOne();
    return record != null;
  }
}
